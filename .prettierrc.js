module.exports = {
    endOfLine: 'auto',
    tabWidth: 4,
    useTabs: false,
    singleQuote: true,
};
